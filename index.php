<?php
/**
 * Rover Class
 *
 * We have map and (x, y) coordinates system
 *
 * We have 2 types of direction here:
 *          - step command direction: depends on camera direction;
 *          - camera direction: depends on axis on the map;
 *
 * set Directions:
 *          - 0: step FORWARD command OR make one step UP by y axis on map (make +y);
 *          - 1: step RIGHT command OR make one step RIGHT by x axis on map (make +x);
 *          - 2: step BACKWARD command OR make one step DOWN by y axis (make -y);
 *          - 3: step LEFT command OR make one step LEFT by x axis (make -x);
 *
 * set camera direction by default (on the start): 0, so our camera direction is up by y axis
 * set position: counts steps from the origin (x:0, y:0)
 */

class Rover {
    private int $cameraDirection;
    private array $position;

    const AXIS_INCREASE = 1;
    const AXIS_DECREASE = -1;

    const FORWARD = 0;
    const RIGHT = 1;
    const BACKWARD = 2;
    const LEFT = 3;

    private array $directions = [self::FORWARD, self::RIGHT, self::BACKWARD, self::LEFT];

    public function __construct(int $cameraDirection = self::FORWARD)
    {
        $this->position = ['x' => 0, 'y' => 0];
        $this->cameraDirection = $cameraDirection;
    }

    // get camera direction
    public function getCameraDirection(): int
    {
        return  $this->cameraDirection;
    }

    // get rover position
    public function getPosition(): array
    {
        return $this->position;
    }

    // make step regarding to direction
    public function makeStep(int $moveDirection): bool
    {
        // wrong command. Stay as is
        if ($moveDirection < 0 || $moveDirection >= count($this->directions)) {
            return false;
        }

        // turn camera into given direction
        $this->turnCamera($moveDirection);

        // get move direction by axis
        $axisDirection = ($this->cameraDirection == self::LEFT || $this->cameraDirection == self::BACKWARD)
            ? self::AXIS_DECREASE
            : self::AXIS_INCREASE;

        // invert direction if we need to make step backward
        if ($moveDirection == self::BACKWARD) {
            $axisDirection *= -1;
        }

        // make step
        match ($this->cameraDirection) {
            self::FORWARD, self::BACKWARD => $this->stepByY($axisDirection),
            self::LEFT, self::RIGHT => $this->stepByX($axisDirection),
        };

        return true;
    }

    // make step by x axis regarding to move direction
    private function stepByX(int $axisDirection): void
    {
        $this->position['x'] += $axisDirection;
    }

    // make step by y axis regarding to move direction
    private function stepByY(int $axisDirection): void
    {
        $this->position['y'] += $axisDirection;
    }

    // turn camera
    private function turnCamera(int $moveDirection): void
    {
        switch ($moveDirection) {
            case self::RIGHT:
                $this->cameraDirection = ($this->cameraDirection + 1) % count($this->directions);
                break;
            case self::LEFT:
                $this->cameraDirection = ($this->cameraDirection - 1 + count($this->directions)) % count($this->directions);
                break;
        }
    }
}

// make tests
// constants were made for good visibility
const FORWARD = 0;
const RIGHT = 1;
const BACKWARD = 2;
const LEFT = 3;

$rover = new Rover();
$steps = [BACKWARD, FORWARD, FORWARD, LEFT, RIGHT, LEFT, LEFT, LEFT, BACKWARD];

foreach ($steps as $step) {
    $roverStep = $rover->makeStep($step);
    $position = $rover->getPosition();
    echo "<p>Rover position is ({$position['x']}, {$position['y']})</p>";

    $cameraDirectionText = match ($rover->getCameraDirection()) {
        0 => 'up',
        1 => 'right',
        2 => 'down',
        3 => 'left',
    };
    echo "<p>Camera is looking $cameraDirectionText</p>";
    echo "<hr>";
}
?>
